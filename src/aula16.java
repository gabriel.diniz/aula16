import java.util.Scanner;

public class aula16 { // constante 2 - distancia que o som vai percorrer num determinado tempo

    public static void main(String[] args) {

        final double vsom = 340.28; // metros por segundo m/s - constante velocidade do som

        System.out.println("Digite o espaço de tempo: ");  // imprime a mensagem

        Scanner in = new Scanner(System.in); // faz a leitura do que foi digitado

        int tempo = in.nextInt();  // variavel inteira tempo

        System.out.println("A distância seria de: "+tempo * vsom + " metros percorridos."); // imprime o resultado de tempo

        System.out.println("A distância km seria de: "+(tempo * vsom) / 1000 + " kms percorridos."); // imprime o resultado de tempo em kms

    }
}
